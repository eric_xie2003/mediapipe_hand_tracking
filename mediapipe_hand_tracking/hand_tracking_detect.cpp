#include <vector>

#include "hand_tracking_detect.h"
#include "hand_gesture_recognition.h"
#include "hand_up_hand_down_detect.h"


GoogleMediapipeHandTrackingDetect::HandTrackingDetect::HandTrackingDetect()
{
	m_bIsInit = false;
	m_bIsRelease = false;
	m_kInputStream = "input_video";
	m_kOutputStream = "output_video";
	m_kWindowName = "MediaPipe";
	m_kOutputLandmarks = "landmarks";
	m_LandmarksCallBackFunc = nullptr;
	m_GestureResultCallBackFunc = nullptr;
}

GoogleMediapipeHandTrackingDetect::HandTrackingDetect::~HandTrackingDetect()
{
	if (!m_bIsRelease)
	{
		Release();
	}
}

int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::InitGraph(const char* model_path)
{
	absl::Status run_status = Mediapipe_InitGraph(model_path);
	if (!run_status.ok()) 
	{
		return 0;
	}
	m_bIsInit = true;
	return  1;
}


int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::RegisterLandmarksCallback(LandmarksCallBack func)
{
	if (func != nullptr)
	{
		m_LandmarksCallBackFunc = func;
		return 1;
	}

	return 0;
}

int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::RegisterGestureResultCallBack(GestureResultCallBack func)
{
	if (func != nullptr)
	{
		m_GestureResultCallBackFunc = func;
		return 1;
	}

	return 0;
}


int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::DetectFrame(int image_index, int image_width, int image_height, void* image_data)
{
	if (!m_bIsInit)
		return 0;

	absl::Status run_status = Mediapipe_RunMPPGraph(image_index,image_width,image_height,image_data);
	if (!run_status.ok()) {
		return 0;
	}
	return 1;
}

int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::DetectFrameDirect(int image_width, int image_height, void* image_data, GestureRecognitionResult& gesture_result)
{
	if (!m_bIsInit)
		return 0;

	absl::Status run_status = Mediapipe_RunMPPGraphDirect(image_width, image_height, image_data, gesture_result);
	if (!run_status.ok()) {
		return 0;
	}
	return 1;
}

int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::DetectVideo(int device)
{
	if (!m_bIsInit)
		return 0;

	if (m_tVideoThread.joinable())
	{
		m_tVideoThread.detach();
	}

	m_tVideoThread = std::thread(VideoMPPGraph, this, device);

	return 1;

	// absl::Status run_status = Mediapipe_RunMPPGraph(video_path, show_image);
	// if (!run_status.ok()) {
	// 	return 0;
	// }
	// return 1;
}


int GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Release()
{
	absl::Status run_status = Mediapipe_ReleaseGraph();
	if (!run_status.ok()) {
		return 0;
	}
	return 1;
}


absl::Status GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Mediapipe_InitGraph(const char* model_path)
{
	std::string calculator_graph_config_contents;
	MP_RETURN_IF_ERROR(mediapipe::file::GetContents(model_path, &calculator_graph_config_contents));

	mediapipe::CalculatorGraphConfig config =
		mediapipe::ParseTextProtoOrDie<mediapipe::CalculatorGraphConfig>(
			calculator_graph_config_contents);

	MP_RETURN_IF_ERROR(m_Graph.Initialize(config));

	// Add video Output Stream
	auto sop = m_Graph.AddOutputStreamPoller(m_kOutputStream);
	assert(sop.ok());
	m_pPoller = std::make_unique<mediapipe::OutputStreamPoller>(std::move(sop.value()));

	// Add landmarks Output Stream
	mediapipe::StatusOrPoller sop_landmark = m_Graph.AddOutputStreamPoller(m_kOutputLandmarks);
	assert(sop_landmark.ok());
	m_pPoller_landmarks = std::make_unique<mediapipe::OutputStreamPoller>(std::move(sop_landmark.value()));

	MP_RETURN_IF_ERROR(m_Graph.StartRun({}));

	return absl::OkStatus();
}

absl::Status GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Mediapipe_RunMPPGraph(int image_index, int image_width, int image_height, void* image_data)
{
	// construct cv::Mat object
	cv::Mat camera_frame(cv::Size(image_width, image_height), CV_8UC3,(uchar*)image_data);
	cv::cvtColor(camera_frame, camera_frame, cv::COLOR_BGR2RGB);
	cv::flip(camera_frame, camera_frame, /*flipcode=HORIZONTAL*/ 1);
	//std::cout << "Image construct finished." << std::endl;

	// Wrap Mat into an ImageFrame.
	auto input_frame = absl::make_unique<mediapipe::ImageFrame>(
		mediapipe::ImageFormat::SRGB, camera_frame.cols, camera_frame.rows,
		mediapipe::ImageFrame::kDefaultAlignmentBoundary);
	cv::Mat input_frame_mat = mediapipe::formats::MatView(input_frame.get());
	camera_frame.copyTo(input_frame_mat);
	//std::cout << "Wrap Mat into an ImageFrame." << std::endl;

	// Send image packet into the graph.
	size_t frame_timestamp_us =
		(double)cv::getTickCount() / (double)cv::getTickFrequency() * 1e6;

	MP_RETURN_IF_ERROR(m_Graph.AddPacketToInputStream(
		m_kInputStream, mediapipe::Adopt(input_frame.release())
		.At(mediapipe::Timestamp(frame_timestamp_us))));
	//std::cout << "Send image packet into the graph." << std::endl;


	// Get the graph result packet, or stop if that fails.
	mediapipe::Packet packet;
	mediapipe::Packet packet_landmarks;
	if (!m_pPoller->Next(&packet)) 
		return absl::OkStatus();

	if (m_pPoller_landmarks->QueueSize() > 0) 
	{
		if (m_pPoller_landmarks->Next(&packet_landmarks))
		{

			std::vector<mediapipe::NormalizedLandmarkList> output_landmarks = packet_landmarks.Get<std::vector<mediapipe::NormalizedLandmarkList>>();

			// Hand Landmarks callback
			if (m_LandmarksCallBackFunc)
			{
				// Get the landmarks and store to hand_landmarks
				std::vector<HandLandMarks> hand_landmarks;
				hand_landmarks.clear();
				for (int m = 0; m < output_landmarks.size(); ++m)
				{
					mediapipe::NormalizedLandmarkList single_hand_NormalizedLandmarkList = output_landmarks[m];
					for (int i = 0; i < single_hand_NormalizedLandmarkList.landmark_size(); ++i)
					{
						HandLandMarks info;
						const mediapipe::NormalizedLandmark landmark = single_hand_NormalizedLandmarkList.landmark(i);
						info.x = landmark.x();
						info.y = landmark.y();
						info.z = landmark.z();
						hand_landmarks.push_back(info);
					}
				}

				// callback
				HandLandMarks* hand_landmarks_pose_infos = new HandLandMarks[hand_landmarks.size()];
				for (int i = 0; i < hand_landmarks.size(); ++i)
				{
					hand_landmarks_pose_infos[i].x = hand_landmarks[i].x;
					hand_landmarks_pose_infos[i].y = hand_landmarks[i].y;
					hand_landmarks_pose_infos[i].z = hand_landmarks[i].z;
				}
				m_LandmarksCallBackFunc(image_index, hand_landmarks_pose_infos, hand_landmarks.size());
				delete[] hand_landmarks_pose_infos;
			}


			// Detect Gesture Result
			if (m_GestureResultCallBackFunc)
			{
				int* hand_gesture_recognition_result = new int[output_landmarks.size()];

				for (int m = 0; m < output_landmarks.size(); ++m)
				{
					// Get single hand landmarks and store to singleHandGestureInfo
					mediapipe::NormalizedLandmarkList single_hand_NormalizedLandmarkList = output_landmarks[m];
					std::vector<PoseInfo> singleHandGestureInfo;
					singleHandGestureInfo.clear();
					for (int i = 0; i < single_hand_NormalizedLandmarkList.landmark_size(); ++i)
					{
						PoseInfo info;
						const mediapipe::NormalizedLandmark landmark = single_hand_NormalizedLandmarkList.landmark(i);
						info.x = landmark.x() * camera_frame.cols;
						info.y = landmark.y() * camera_frame.rows;
						singleHandGestureInfo.push_back(info);
					}

					// Recognize the gesture of the single hand
					HandGestureRecognition handGestureRecognition;
					int result = handGestureRecognition.GestureRecognition(singleHandGestureInfo);
					hand_gesture_recognition_result[m] = result;

				}

				// callback
				m_GestureResultCallBackFunc(image_index, hand_gesture_recognition_result, output_landmarks.size());
				delete[] hand_gesture_recognition_result;
			}
		}
	}

	return absl::OkStatus();
}


absl::Status GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Mediapipe_RunMPPGraphDirect(int image_width, int image_height, void* image_data, GestureRecognitionResult& gesture_result)
{
	// construct cv::Mat object
	cv::Mat camera_frame(cv::Size(image_width, image_height), CV_8UC3, (uchar*)image_data);
	cv::cvtColor(camera_frame, camera_frame, cv::COLOR_BGR2RGB);
	cv::flip(camera_frame, camera_frame, /*flipcode=HORIZONTAL*/ 1);
	//std::cout << "Image construct finished." << std::endl;

	// Wrap Mat into an ImageFrame.
	auto input_frame = absl::make_unique<mediapipe::ImageFrame>(
		mediapipe::ImageFormat::SRGB, camera_frame.cols, camera_frame.rows,
		mediapipe::ImageFrame::kDefaultAlignmentBoundary);
	cv::Mat input_frame_mat = mediapipe::formats::MatView(input_frame.get());
	camera_frame.copyTo(input_frame_mat);
	//std::cout << "Wrap Mat into an ImageFrame." << std::endl;

	// Send image packet into the graph.
	size_t frame_timestamp_us =
		(double)cv::getTickCount() / (double)cv::getTickFrequency() * 1e6;

	MP_RETURN_IF_ERROR(m_Graph.AddPacketToInputStream(
		m_kInputStream, mediapipe::Adopt(input_frame.release())
		.At(mediapipe::Timestamp(frame_timestamp_us))));
	//std::cout << "Send image packet into the graph." << std::endl;


	// Get the graph result packet, or stop if that fails.
	mediapipe::Packet packet;
	mediapipe::Packet packet_landmarks;
	if (!m_pPoller->Next(&packet))
		return absl::OkStatus();

	if (m_pPoller_landmarks->QueueSize() > 0)
	{
		if (m_pPoller_landmarks->Next(&packet_landmarks))
		{

			std::vector<mediapipe::NormalizedLandmarkList> output_landmarks = packet_landmarks.Get<std::vector<mediapipe::NormalizedLandmarkList>>();

			GestureRecognitionResult tempGestureResult;

			for (int m = 0; m < output_landmarks.size(); ++m)
			{
				mediapipe::NormalizedLandmarkList single_hand_NormalizedLandmarkList = output_landmarks[m];

				std::vector<PoseInfo> singleHandGestureInfo;
				singleHandGestureInfo.clear();

				for (int i = 0; i < single_hand_NormalizedLandmarkList.landmark_size(); ++i)
				{
					PoseInfo info;
					const mediapipe::NormalizedLandmark landmark = single_hand_NormalizedLandmarkList.landmark(i);
					info.x = landmark.x() * camera_frame.cols;
					info.y = landmark.y() * camera_frame.rows;
					singleHandGestureInfo.push_back(info);
				}

				// Detect Gesture
				HandGestureRecognition handGestureRecognition;
				int gesture_recognition_result = handGestureRecognition.GestureRecognition(singleHandGestureInfo);
				tempGestureResult.m_Gesture_Recognition_Result[m] = gesture_recognition_result;

				// Detect Hand Up or Down
				HandUpHandDownDetect handupHandDownDetect;
				int handuphanddown_result = handupHandDownDetect.DetectHandUpOrHandDown(singleHandGestureInfo, image_height);
				tempGestureResult.m_HandUp_HandDown_Detect_Result[m] = handuphanddown_result;
				
			}

			gesture_result = tempGestureResult;

		}
	}

	return absl::OkStatus();
}



absl::Status GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Mediapipe_RunMPPGraphCamera(int device)
{

	cv::VideoCapture capture;
	capture.open(device);
	RET_CHECK(capture.isOpened());
	
	std::cout << "capture.isOpened()" << std::endl;

#if (CV_MAJOR_VERSION >= 3) && (CV_MINOR_VERSION >= 2)
	capture.set(cv::CAP_PROP_FRAME_WIDTH, 640);
	capture.set(cv::CAP_PROP_FRAME_HEIGHT, 480);
	capture.set(cv::CAP_PROP_FPS, 30);
#endif

	int image_index = 0;
	while (!m_bIsRelease) {
		// Capture opencv camera or video frame.
		cv::Mat camera_frame_raw;
		capture >> camera_frame_raw;
		if (camera_frame_raw.empty())
			break;

		cv::Mat camera_frame;
		cv::cvtColor(camera_frame_raw, camera_frame, cv::COLOR_BGR2RGB);
		cv::flip(camera_frame, camera_frame, /*flipcode=HORIZONTAL*/ 1);

		// Wrap Mat into an ImageFrame.
		auto input_frame = absl::make_unique<mediapipe::ImageFrame>(
			mediapipe::ImageFormat::SRGB, camera_frame.cols, camera_frame.rows,
			mediapipe::ImageFrame::kDefaultAlignmentBoundary);
		cv::Mat input_frame_mat = mediapipe::formats::MatView(input_frame.get());
		camera_frame.copyTo(input_frame_mat);

		// Send image packet into the graph.
		size_t frame_timestamp_us =
			(double)cv::getTickCount() / (double)cv::getTickFrequency() * 1e6;

		MP_RETURN_IF_ERROR(m_Graph.AddPacketToInputStream(
			m_kInputStream, mediapipe::Adopt(input_frame.release())
			.At(mediapipe::Timestamp(frame_timestamp_us))));

		// Get the graph result packet, or stop if that fails.
		mediapipe::Packet packet;
		mediapipe::Packet packet_landmarks;
		if (!m_pPoller->Next(&packet)) break;
		if (m_pPoller_landmarks->QueueSize() > 0) {
			if (m_pPoller_landmarks->Next(&packet_landmarks))
			{

				std::vector<mediapipe::NormalizedLandmarkList> output_landmarks = packet_landmarks.Get<std::vector<mediapipe::NormalizedLandmarkList>>();

				// Hand Landmarks callback
				if (m_LandmarksCallBackFunc)
				{
					// Get the landmarks and store to hand_landmarks
					std::vector<HandLandMarks> hand_landmarks;
					hand_landmarks.clear();
					for (int m = 0; m < output_landmarks.size(); ++m)
					{
						mediapipe::NormalizedLandmarkList single_hand_NormalizedLandmarkList = output_landmarks[m];
						for (int i = 0; i < single_hand_NormalizedLandmarkList.landmark_size(); ++i)
						{
							HandLandMarks info;
							const mediapipe::NormalizedLandmark landmark = single_hand_NormalizedLandmarkList.landmark(i);
							info.x = landmark.x();
							info.y = landmark.y();
							info.z = landmark.z();
							hand_landmarks.push_back(info);
						}
					}

					// callback
					HandLandMarks* hand_landmarks_pose_infos = new HandLandMarks[hand_landmarks.size()];
					for (int i = 0; i < hand_landmarks.size(); ++i)
					{
						hand_landmarks_pose_infos[i].x = hand_landmarks[i].x;
						hand_landmarks_pose_infos[i].y = hand_landmarks[i].y;
						hand_landmarks_pose_infos[i].z = hand_landmarks[i].z;
					}
					m_LandmarksCallBackFunc(image_index, hand_landmarks_pose_infos, hand_landmarks.size());
					delete[] hand_landmarks_pose_infos;
				}


				// Detect Gesture Result
				if (m_GestureResultCallBackFunc)
				{
					int* hand_gesture_recognition_result = new int[output_landmarks.size()];

					for (int m = 0; m < output_landmarks.size(); ++m)
					{
						// Get single hand landmarks and store to singleHandGestureInfo
						mediapipe::NormalizedLandmarkList single_hand_NormalizedLandmarkList = output_landmarks[m];
						std::vector<PoseInfo> singleHandGestureInfo;
						singleHandGestureInfo.clear();
						for (int i = 0; i < single_hand_NormalizedLandmarkList.landmark_size(); ++i)
						{
							PoseInfo info;
							const mediapipe::NormalizedLandmark landmark = single_hand_NormalizedLandmarkList.landmark(i);
							info.x = landmark.x() * camera_frame.cols;
							info.y = landmark.y() * camera_frame.rows;
							singleHandGestureInfo.push_back(info);
						}

						// Recognize the gesture of the single hand
						HandGestureRecognition handGestureRecognition;
						int result = handGestureRecognition.GestureRecognition(singleHandGestureInfo);
						hand_gesture_recognition_result[m] = result;

					}

					// callback
					m_GestureResultCallBackFunc(image_index, hand_gesture_recognition_result, output_landmarks.size());
					delete[] hand_gesture_recognition_result;
				}
			}

		}

		image_index += 1;
	}

	capture.release();
	return absl::OkStatus();
}


absl::Status GoogleMediapipeHandTrackingDetect::HandTrackingDetect::Mediapipe_ReleaseGraph()
{
	m_bIsRelease = true;
	if (m_tVideoThread.joinable())
	{
		m_tVideoThread.join();
	}
	MP_RETURN_IF_ERROR(m_Graph.CloseInputStream(m_kInputStream));
	return m_Graph.WaitUntilDone();
}