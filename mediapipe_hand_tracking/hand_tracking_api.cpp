#include "hand_tracking_api.h"
#include "hand_tracking_detect.h"

using namespace GoogleMediapipeHandTrackingDetect;

HandTrackingDetect m_HandTrackingDetect;

EXPORT_API int MediapipeHandTrackingInit(const char* model_path)
{
	return m_HandTrackingDetect.InitGraph(model_path);
}


EXPORT_API int MediapipeHandTrackingReigeterLandmarksCallback(LandmarksCallBack func)
{
	return m_HandTrackingDetect.RegisterLandmarksCallback(func);
}

EXPORT_API int MediapipeHandTrackingRegisterGestureResultCallback(GestureResultCallBack func)
{
	return m_HandTrackingDetect.RegisterGestureResultCallBack(func);
}

EXPORT_API int MediapipeHandTrackingDetectFrame(int image_index, int image_width, int image_height, void* image_data)
{
	return m_HandTrackingDetect.DetectFrame(image_index, image_width, image_height, image_data);
}

EXPORT_API int MediapipeHandTrackingDetectFrameDirect(int image_width, int image_height, void* image_data,GestureRecognitionResult& gesture_result)
{
	return m_HandTrackingDetect.DetectFrameDirect(image_width, image_height, image_data, gesture_result);
}

EXPORT_API int MediapipeHandTrackingDetectVideo(int device)
{
	return m_HandTrackingDetect.DetectVideo(device);
}


EXPORT_API int MediapipeHandTrackingRelease()
{
	return m_HandTrackingDetect.Release();
}