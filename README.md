# Mediapipe Hand Tracking


### 项目简介

将Google Mediapipe中的手部追踪与识别功能封装成动态链接库，通过调用该库可以在桌面应用程序中进行手势识别以及得到手部关节坐标点。

参考[Mediapipe – 将Mediapipe handtracking封装成动态链接库dll/so,实现在桌面应用中嵌入手势识别功能](https://www.stubbornhuang.com/1562/)的文档实现。

将hand_tracking封装成库的代码参见[github](https://github.com/HW140701/Google_Mediapipe_Hand_Tracking_dll)。

Windows下的编译请参考上面的文档，本文档仅介绍如何在MacOS下编译和运行hand tracking。

### 代码下载

1. 下载mediapipe_hand_tracking源码
```
git clone https://gitee.com/xuanyishenzhen/mediapipe_hand_tracking.git
```

2.下载mediapipe源码
```
git clone https://github.com/google/mediapipe.git
```


### 编译方法：

一、编译动态库

1. 将`mediapipe_hand_tracking`目录中的`mediapipe_hand_tracking`拷贝到```mediapipe/mediapipe/example/desktop/```目录中
```
cp -r mediapipe_hand_tracking/mediapipe_hand_tracking mediapipe/mediapipe/example/desktop/
```
2. 编译
```
cd mediapipe
bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 mediapipe/examples/desktop/mediapipe_hand_tracking:MediapipeHandTracking --verbose_failures
```
注意：编译过程中如果报头文件找不到的错误，说明依赖有误，需要先查找相应头文件在哪个包中，然后编辑BUILD文件，在deps中增加相应的包即可。

二、编译测试程序

3. 将生成的动态库`libMediapipeHandTracking.so`（在`bazel-bin/mediapipe/examples/desktop/mediapipe_hand_tracking`目录）复制到此项目的`lib`目录中
```
cp bazel-bin/mediapipe/examples/desktop/mediapipe_hand_tracking/libMediapipeHandTracking.so ../mediapipe_hand_tracking/lib
```

4. 编译测试程序
     ```
     cd mediapipe_hand_tracking/build
     cmake ..
     make
     ```

5. 创建动态库目录
     ```
     mkdir -p bazel-out/darwin-opt/bin/mediapipe/examples/desktop/mediapipe_hand_tracking
     ```

6. 将动态库`libMediapipeHandTracking.so`复制到该目录
     ```
     cp ../lib/libMediapipeHandTracking.so bazel-out/darwin-opt/bin/mediapipe/examples/desktop/mediapipe_hand_tracking/
     ```

三、运行

5. 将`hand_tracking_desktop_live.pbtxt`复制到可执行文件所在的目录(`build`)。

6. 创建保存模型文件的目录
     ```
     mkdir -p mediapipe/modules
     ```

7. 从mediapipe项目中将`hand_landmark`和`palm_detection`两个目录复制到`mediapipe/modules`目录中

8. 运行
    ```
    ./MediapipeHandTracking
    ```